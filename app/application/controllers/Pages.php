<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {
    
    public function index() {
        
        //redirect('coming-soon', 'refresh');
        
        $this->load->view('_blocks/header');
        $this->load->view('pages/index');
        $this->load->view('_blocks/footer');
    }

    

}
