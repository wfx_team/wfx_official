<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/jquerysctipttop.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/style.css">
<!-- //Style -->

<!-- Fonts -->
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
<!-- Fonts -->