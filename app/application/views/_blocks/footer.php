<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="page-section footer-section pad-top-half pad-bottom-half lighter-black-bg">

  <div class="container-fluid no-padding">
    <div class="row-fluid">
      <article class="col-md-12 no-padding">
        <div class="footer-wrap">

          <div class="footer-name">
            <h1 class="grey font-size-35 uppercase normal-font">all rights reserved</h1>
            <h1 class="grey font2 uppercase normal-font">Copyright WFX&#169; 2016 </h1>
          </div> 
            

          <div class="social-icons text-right">
            <ul>
              <li>
                <a href="#" title="Facebook"><i class="ion-social-facebook grey ease"></i></a>
              </li>
              <li>
                <a href="#" title="Google-Plus"><i class="ion-social-googleplus grey ease"></i></a>
              </li>
              <li>
                <a href="#" title="Linkedin"><i class="ion-social-linkedin grey ease"></i></a>
              </li>
              <li>
                <a href="#" title="Pinterest"><i class="ion-social-pinterest grey ease"></i></a>
              </li>
            </ul>
          </div>

        </div>        
      </article>
    </div>
  </div>
    
</section>


</section>
<!-- Master Wrap : ends -->

<!-- Core JS Libraries -->
<script src="<?php echo base_url(); ?>assets/js/libs/jquery.min.js" type="text/javascript"></script>


<!-- JS Plugins -->
<script src="<?php echo base_url(); ?>assets/js/libs/library-imports.js"></script>

<!-- JS Custom Codes --> 
<script src="<?php echo base_url(); ?>assets/js/custom/main.js" ></script> 

<!-- google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgI2INauG3s25_JwJb-bMRLtburmEsdSc" type="text/javascript"></script> 
        
        <script type="text/javascript">
    var locations = [
      ['WFX Head Office', 7.321934, 80.649730, 2]
    ];

  
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 17,
      center: new google.maps.LatLng(7.321934, 80.649730),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
       styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#cdbfa1"}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels.text","stylers":[{"color":"#4c4c4c"},{"saturation":"0"},{"visibility":"simplified"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#d7d6d7"},{"saturation":"0"}]},{"featureType":"road","elementType":"labels.text","stylers":[{"color":"#4c4c4c"},{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#3999d8"}]}]
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
     
     icon:'<?php echo base_url(); ?>assets/images/img/placeholder.png',
        map: map
      });

   
    
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    
    

    
    }
  </script>

<!-- end google map -->

<!--page smooth scroll-->
<script>
  
 if (window.addEventListener) window.addEventListener('DOMMouseScroll', wheel, false);
window.onmousewheel = document.onmousewheel = wheel;

function wheel(event) {
    var delta = 0;
    if (event.wheelDelta) delta = event.wheelDelta / 24;
    else if (event.detail) delta = -event.detail / 3;

    handle(delta);
    if (event.preventDefault) event.preventDefault();
    event.returnValue = false;
}

var goUp = true;
var end = null;
var interval = null;

function handle(delta) {
  var animationInterval = 20; //lower is faster
  var scrollSpeed = 20; //lower is faster

  if (end == null) {
    end = $(window).scrollTop();
  }
  end -= 10 * delta;
  goUp = delta > 0;

  if (interval == null) {
    interval = setInterval(function () {
      var scrollTop = $(window).scrollTop();
      var step = Math.round((end - scrollTop) / scrollSpeed);
      if (scrollTop <= 0 || 
          scrollTop >= $(window).prop("scrollHeight") - $(window).height() ||
          goUp && step > -1 || 
          !goUp && step < 1 ) {
        clearInterval(interval);
        interval = null;
        end = null;
      }
      $(window).scrollTop(scrollTop + step );
    }, animationInterval);
  }
}


    </script>
<!--End page smooth scroll-->
</body>

</html>