<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta charset="utf-8">
<title>WFX | Web design Sri Lanka</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Standard Favicon--> 
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png">

<!-- Bootstrap CSS Files -->
<link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">

<!-- Custom Fonts Setup -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/ionicons/css/ionicons.css" >

<!-- CSS files for plugins -->
<link href="<?php echo base_url(); ?>assets/css/library-imports.css" rel="stylesheet">

<!-- Main Template Styles -->
<link href="<?php echo base_url(); ?>assets/css/home-04.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/iecustom.css" rel="stylesheet">

<!-- Main Template Responsive Styles -->
<link href="<?php echo base_url(); ?>assets/css/main-responsive.css" rel="stylesheet">
<!-- Main Template Retina Optimizaton Rules -->
<link href="<?php echo base_url(); ?>assets/css/main-retina.css" rel="stylesheet">
<!-- LESS stylesheet for managing color presets -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/less/color.css">
<!-- LESS stylesheet for managing font presets -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/less/font.css">

<!-- Google Web Fonts-->
<link href='https://fonts.googleapis.com/css?family=Roboto:400,700%7CPT+Sans:400,700' rel='stylesheet' type='text/css'>
<!-- <link href='https://fonts.googleapis.com/css?family=Roboto:900' rel='stylesheet' type='text/css'> -->

</head>