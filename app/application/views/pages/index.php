<body>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>


    <!-- Sliding Navigation : starts -->
    <div class="main-vertical-nav-wrap">
        <div class="vertical-nav-trigger">
            <div id="menu-toggle">
                <div id="hamburger" class="">
                    <span class="white-bg color"></span>
                    <span class="white-bg color"></span>
                    <span class="white-bg color"></span>
                </div>
                <div id="cross">
                    <span class="white-bg color"></span>
                    <span class="white-bg color"></span>
                </div>
            </div>
        </div>
        <div class="vertical-nav-container">

            <!-- Logo Section -->
            <div class="vertical-logo-section section-bgimg" data-sectionBgImg="<?php echo base_url(); ?>assets/images/logo-bgimg.jpg">
                <div class="vertical-align text-center">
                    <img class="img-responsive" src="<?php echo base_url(); ?>assets/images/img/logo1.png" alt="Adam Creative" data-no-retina>
                </div> 
            </div>

            <!-- Menu Section -->
            <div class="vertical-menu-section vertical-menu text-center light-black-bg">
                <div class="vertical-align">

                    <ul class="main-menu">
                        <!-- Home Sections -->
                        <li>
                            <a class="scroll-link white font1" href="<?php echo base_url(); ?>">Home</a>
                        </li>

                        <li>
                            <a class="scroll-link white font1" href="#about">About</a>
                        </li>

                        <li>
                            <a class="scroll-link white font1" href="#services">Services</a>
                        </li>

                        <li>
                            <a class="scroll-link white font1" href="#portfolio">Portfolio</a>
                        </li>

                        <li>
                            <a class="scroll-link white font1" href="#contact">Contact</a>
                        </li>

                    </ul>

                </div>  
            </div>
            <div class="clear-float"></div>
        </div>
    </div>
    <!-- Sliding Navigation : ends -->

    <!-- To Top Button -->
    <div class="mach-to-top main-subtle-bg">
        <a class="scroll-link" href="#mastwrap"><i class="white icon ion-ios-arrow-up"></i></a>
    </div>

    <!-- Master Wrap : starts -->
    <section id="mastwrap" class="mastwrap">


        <div class="home-04">
            <div class="home-04-wrap black-bg fullheight-and-onefourth section-bgimg background-fixed fade-on-scroll-wrap" data-sectionBgImg="assets/images/img/home.jpg">
                <div class="vertical-align">
                    <div class="container">
                        <div class="row">
                            <article class="col-md-8">

                                <div class="heading-section fader">
                                    <img src="<?php echo base_url(); ?>assets/images/img/logo.png" class="logo-img img-responsive" alt="WFX">
                                    <!-- <h1 class="dark-white font2 uppercase normal-font">WFX</h1> -->
                                    <h3 class="black font2 uppercase normal-font">Web Design And Creative Solutions Sri Lanka</h3>

                                    <section class="cd-intro text-left">
                                        <h1 class="cd-headline push dark-white font2 uppercase normal-font">
                                            <span class="cd-words-wrapper">
                                                <b class="is-visible">Web Design and Development</b>
                                                <b>Search Engine Optimization</b>
                                                <b>Digital Marketing</b>
                                                <b>Professional Photography</b>
                                                <b>eCommerce Solutions</b>
                                                <b>Software Solutions</b>
                                            </span>
                                        </h1>
                                    </section> <!-- cd-intro -->

                                </div>

                            </article>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- About Section Starts -->
        <section id="about" class="page-section section-fixed-header-wrap about-section white-bg">

            <!-- Fixed Header Section Starts -->
            <div class="fixed-header fullheight">

                <!-- SVG Section Starts -->
                <svg class="parent" xmlns="https://www.w3.org/2000/svg" version="1.1"  xmlns:xlink="https://www.w3.org/1999/xlink"> 
                <!-- add title for accessibility -->
                <title>Mach SVG</title> 
                <!-- Definition Section Of the SVG -->
                <defs>  
                <clippath id="my-path01">
                    <text class="letter-text font1" y="57%">A</text>
                    <text class="letter-text-ie font1 hidden" x="50%" y="57%">A</text>
                    <!-- <text class="header-text font1" x="50%" y="65%">About Us</text> -->
                </clippath>
                </defs>
                <!-- The background Image for the svg font -->
                <image xlink:href="<?php echo base_url(); ?>assets/images/img/about.jpg" clip-path="url(#my-path01)" width="100%" height="100%" preserveAspectRatio="xMidYMid slice" />
                </svg>
                <!-- SVG Section Ends -->

                <p class="header-text light-more-black font2">About Us</p>

            </div>
            <!-- Fixed Header Section Ends -->

            <div class="fixed-aside-section white-bg">
                <div class="vertical-align main-aligner">
                    <div class="content-section pad-top-half pad-bottom-half">
                        <div class="container">
                            <div class="row">
                                <article class="col-md-12">
                                    <div class="main-heading">
                                        <h1 class="black font2 uppercase normal-font"><span class="main">WFX </span> Is An awesome digital agency</h1>
                                    </div>
                                    <p class="content-type-one pad-min-top-quarter grey font1 normal-font text-justify">
                                        WFX web designing company always with our clients and help them to achieve their   business and technology needs. We have excellent knowledge, experience and skills for giving best service & solutions to our clients.

                                    </p> <br/>
                                    <p class="content-type-one grey font1 normal-font text-justify">
                                        Our company provide a wide range of it and interrelated services, inclusive of business consultancy, designing and developing software solutions, hosting of web sites and web based systems, electronic marketing and ,advertising etc 
                                    </p> 


                                </article>
                            </div>

                            <div class="row">
                                <article class="col-md-6 col-sm-6 pad-top-half">
                                    <div class="our-history">
                                        <div class="heading-type-one">
                                            <h2 class="black font2 uppercase normal-font">Our <span class="main bold-font">History</span></h2>
                                        </div>
                                        <p class="content-type-one pad-min-top-quarter grey font1 normal-font text-justify">
                                            WFX web designing company, was started in 2016 with the purpose of providing professional it related services for the required clients. 
                                        </p> <br/>
                                        <p class="content-type-one grey font1 normal-font text-justify">
                                            Today we provide a wide variety of IT inter-related services, which include business consultancy, design and development of software solutions, hosting of websites, web maintenance, hotel management and reservation, web-based systems, and electronic advertising and marketing all in the best state-of-the-art functionality.
                                        </p>
                                    </div>
                                </article>
                                <article class="col-md-6 col-sm-6 pad-top-half">
                                    <figure>
                                        <img class="img-fullwidth" src="<?php echo base_url(); ?>assets/images/img/history-img.jpg" alt="Mach" data-no-retina>
                                    </figure>
                                </article>
                            </div>

                            <div class="row">
                                <article class="col-md-12">
                                    <div class="our-history">
                                        <div class="heading-type-one">
                                            <h2 class="black font2 uppercase normal-font">Our <span class="main bold-font">Value</span></h2>
                                        </div>
                                        <p class="content-type-one grey font1 normal-font text-justify">1. Quality in everything we do</p>
                                        <p class="content-type-one grey font1 normal-font text-justify">2. Direct and reliable communication</p>
                                        <p class="content-type-one grey font1 normal-font text-justify">3. Superior customer service</p>
                                        <p class="content-type-one grey font1 normal-font text-justify">4. Hiring and training competent IT Professionals</p>
                                        <p class="content-type-one grey font1 normal-font text-justify">5. Ethical working environment</p>
                                        <p class="content-type-one grey font1 normal-font text-justify">6. Our experience in the IT industry, make us truly understand customer needs</p>
                                        <p class="content-type-one grey font1 normal-font text-justify">7. We offer end-to-end solutions that are modular and sync well with existing systems to help your growth and progress</p>
                                        <p class="content-type-one grey font1 normal-font text-justify">8. We have flexible solutions. We develop and customize solutions to suit your own requirements</p>

                                    </div>
                                </article>
                            </div>


                        </div>
                    </div>  
                </div>
            </div>

        </section>
        <!-- About Section Ends -->

        <!-- Intermediate Testimonial Section Starts -->
        <section class="page-section pad-top pad-bottom white-bg">

            <div class="container">
                <div class="row">
                    <article class="col-md-12">
                        <div class="heading-type-one text-center pad-bottom-half">
                            <h2 class="black font2 uppercase normal-font">Latest <span class="main bold-font">Products</span></h2>

                        </div>    

                        <!-- theme carousel owl starts -->
                        <div class="theme-carousel" data-animatein="fadeIn" data-animateout="fadeOut" data-autoplay="true" data-autoplaytimeout="2000" data-autoplayhoverpause="true" data-autoheight="true" data-center="false" data-loop="true" data-itemsdefault="3" data-margin="5" data-stagepadding="0" data-startposition="0" data-slideby="1" data-nav="false" data-dots="false" data-touchdrag="true" data-mousedrag="true" data-xsnumber="1" data-smnumber="2" data-mdnumber="3" >

                            <!--1-->
                            <div class="item">
                                <div class="hover-block-wrap">
                                    <a href="#" class="img-section hover-block-container feather-single">
                                        <img src="<?php echo base_url(); ?>assets/images/img/clients/r-entertrainment.jpg" class="img-responsive img-fullwidth" alt="Adam Creative" data-no-retina>

                                        <div class="hover-visible black-rgba-high-bg">
                                            <h3 class="light-white font2 uppercase bold-font">
                                                rentertainment.lk
                                            </h3>
                                            <h4 class="main font2 uppercase bold-font">Entertainment</h4>
                                        </div>

                                    </a>
                                </div>
                            </div>

                            <!--2-->
                            <div class="item">
                                <div class="hover-block-wrap">
                                    <a href="#" class="img-section hover-block-container feather-single">
                                        <img src="<?php echo base_url(); ?>assets/images/img/clients/ocean-view.jpg" class="img-responsive img-fullwidth" alt="Adam Creative" data-no-retina>

                                        <div class="hover-visible black-rgba-high-bg">
                                            <h3 class="light-white font2 uppercase bold-font">
                                                oceanviewresort.lk
                                            </h3>
                                            <h4 class="main font2 uppercase bold-font">accommodation & leisure</h4>
                                        </div>

                                    </a>
                                </div>
                            </div>

                            <!--3-->                            
                            <div class="item">
                                <div class="hover-block-wrap">
                                    <a href="#" class="img-section hover-block-container feather-single">
                                        <img src="<?php echo base_url(); ?>assets/images/img/clients/ceylone-living.jpg" class="img-responsive img-fullwidth" alt="Adam Creative" data-no-retina>

                                        <div class="hover-visible black-rgba-high-bg">
                                            <h3 class="light-white font2 uppercase bold-font">
                                                ceylonlivingheritage.com
                                            </h3>
                                            <h4 class="main font2 uppercase bold-font">Travel Agent</h4>
                                        </div>

                                    </a>
                                </div>
                            </div>

                            <!-- 4 -->
                            <div class="item">
                                <div class="hover-block-wrap">
                                    <a href="#" class="img-section hover-block-container feather-single">
                                        <img src="<?php echo base_url(); ?>assets/images/img/clients/helahanda.jpg" class="img-responsive img-fullwidth" alt="Adam Creative" data-no-retina>

                                        <div class="hover-visible black-rgba-high-bg">
                                            <h3 class="light-white font2 uppercase bold-font">
                                                helahandafm.lk
                                            </h3>
                                            <h4 class="main font2 uppercase bold-font">Entertainment</h4>
                                        </div>

                                    </a>
                                </div>

                            </div>

                            <!-- 5-->
                            <div class="item">
                                <div class="hover-block-wrap">
                                    <a href="#" class="img-section hover-block-container feather-single">
                                        <img src="<?php echo base_url(); ?>assets/images/img/clients/firstteam.png" class="img-responsive img-fullwidth" alt="Adam Creative" data-no-retina>

                                        <div class="hover-visible black-rgba-high-bg">
                                            <h3 class="light-white font2 uppercase bold-font">
                                                firstteam.lk
                                            </h3>
                                            <h4 class="main font2 uppercase bold-font">Real Estate</h4>
                                        </div>

                                    </a>
                                </div>

                            </div>

                            <!-- 6 -->
                            <div class="item">
                                <div class="hover-block-wrap">
                                    <a href="#" class="img-section hover-block-container feather-single">
                                        <img src="<?php echo base_url(); ?>assets/images/img/clients/letmeapply.png" class="img-responsive img-fullwidth" alt="Adam Creative" data-no-retina>

                                        <div class="hover-visible black-rgba-high-bg">
                                            <h3 class="light-white font2 uppercase bold-font">
                                                letmeapply.lk
                                            </h3>
                                            <h4 class="main font2 uppercase bold-font">Job Advertisments</h4>
                                        </div>

                                    </a>
                                </div>

                            </div>

                        </div> 
                        <!-- theme carousel owl ends -->

                    </article>
                </div>
            </div>

        </section>

        <!-- Services Section Starts -->
        <section id="services" class="page-section section-fixed-header-wrap top-separator bottom-separator services-section white-bg">

            <!-- Fixed Header Section Starts -->
            <div class="fixed-header fullheight">

                <!-- SVG Section Starts -->
                <svg class="parent" xmlns="https://www.w3.org/2000/svg" version="1.1"  xmlns:xlink="https://www.w3.org/1999/xlink"> 
                <!-- add title for accessibility -->
                <title>Mach SVG</title> 
                <!-- Definition Section Of the SVG -->
                <defs>  
                <clippath id="my-path02">
                    <text class="letter-text font1" y="57%">S</text>
                    <text class="letter-text-ie font1 hidden" x="50%" y="57%">S</text>
                    <!-- <text class="header-text font1" x="50%" y="65%">About Us</text> -->
                </clippath>
                </defs>
                <!-- The background Image for the svg font -->
                <image xlink:href="<?php echo base_url(); ?>assets/images/img/porfolio.jpg" clip-path="url(#my-path02)" width="100%" height="100%" preserveAspectRatio="xMidYMid slice" />
                </svg>
                <!-- SVG Section Ends -->

                <p class="header-text light-more-black font2">Services</p>

            </div>
            <!-- Fixed Header Section Ends -->

            <div class="fixed-aside-section white-bg">
                <div class="vertical-align main-aligner">
                    <div class="content-section pad-top-half pad-bottom-half">
                        <div class="container">

                            <!-- Services Blocks Starts -->
                            <div class="row equi-height-container">

                                <!-- First Services Block -->
                                <article class="col-md-6 col-sm-6 equi-height-each">
                                    <div class="services-each-block section-bgimg">
                                        <div class="services-each-container">
                                            <div class="icon-section">
                                                <i class="ion-ios-browsers-outline lighter-black"></i>
                                            </div>
                                            <div class="heading-section">
                                                <h4 class="lighter-black font2 uppercase bold-font">Web Design And Development</h4>
                                            </div>
                                            <div class="content-section">
                                                <p class="content-type-one grey font1 normal-font">
                                                    WFX has brilliant skills to offer for internet solutions to improve business performance. We have great knowledge, experience and understanding to plan creative and appropriate solutions for your needs, to take the correct message to your target audience. 
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </article>



                                <!-- Second Services Block -->
                                <article class="col-md-6 col-sm-6 equi-height-each">
                                    <div class="services-each-block section-bgimg">
                                        <div class="services-each-container">
                                            <div class="icon-section">
                                                <i class="ion-ios-world-outline lighter-black"></i>
                                            </div>
                                            <div class="heading-section">
                                                <h4 class="lighter-black font2 uppercase bold-font">Search Engine Optimization</h4>
                                            </div>
                                            <div class="content-section">
                                                <p class="content-type-one grey font1 normal-font">
                                                    WFX offers a great attention for Search Engine Optimization service. Increase the number of visitors to a particular website by ensuring that the site appears high on the list of result returned by a search engine.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </article>

                                <!-- Third Services Block -->
                                <article class="col-md-6 col-sm-6 equi-height-each">
                                    <div class="services-each-block section-bgimg">
                                        <div class="services-each-container">
                                            <div class="icon-section">
                                                <i class="ion-ios-briefcase-outline lighter-black"></i>
                                            </div>
                                            <div class="heading-section">
                                                <h4 class="lighter-black font2 uppercase bold-font">Mobile APPs Development</h4>
                                            </div>
                                            <div class="content-section">
                                                <p class="content-type-one grey font1 normal-font">
                                                    WFX is able to develop Mobile Apps that enable your clients to engage with you at their convenience. These Mobile Apps help add value to your products and services, with latest information, best offers, sources and routes of contact, and entertainment.
                                            </div>
                                        </div>
                                    </div>
                                </article>



                                <!-- Fifth Services Block -->
                                <article class="col-md-6 col-sm-6 equi-height-each">
                                    <div class="services-each-block section-bgimg">
                                        <div class="services-each-container">
                                            <div class="icon-section">
                                                <i class="ion-ios-chatboxes-outline lighter-black"></i>
                                            </div>
                                            <div class="heading-section">
                                                <h4 class="lighter-black font2 uppercase bold-font">Social Media Marketing</h4>
                                            </div>
                                            <div class="content-section">
                                                <p class="content-type-one grey font1 normal-font">
                                                    We provide most effective Social Media Applications making it easier for you to interact with your present and would be customers. Social media marketing refers to the process of gaining traffic or attention through social media sites. 

                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </article>

                                <!-- Sixth Services Block -->
                                <article class="col-md-6 col-sm-6 equi-height-each">
                                    <div class="services-each-block section-bgimg">
                                        <div class="services-each-container">
                                            <div class="icon-section">
                                                <i class="ion-ios-monitor-outline lighter-black"></i>
                                            </div>
                                            <div class="heading-section">
                                                <h4 class="lighter-black font1 uppercase bold-font">Software Solutions</h4>
                                            </div>
                                            <div class="content-section">
                                                <p class="content-type-one grey font2 normal-font">
                                                    We develop software solutions, ranging from specific modules and components, to complete, large and complex solutions, which include training, support and global implementation.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </article>

                                <!-- seventh Services Block -->
                                <article class="col-md-6 col-sm-6 equi-height-each">
                                    <div class="services-each-block section-bgimg">
                                        <div class="services-each-container">
                                            <div class="icon-section">
                                                <i class="ion-ios-email-outline lighter-black"></i>
                                            </div>
                                            <div class="heading-section">
                                                <h4 class="lighter-black font1 uppercase bold-font">E-Mail Marketing</h4>
                                            </div>
                                            <div class="content-section">
                                                <p class="content-type-one grey font2 normal-font">
                                                    WFX will increase your sales by using e-mail reach to a huge audience. We create well focused e-mail flyers to registered recipients, with ready expansion of recipients. Our current e-mail reach covers more than 50000 institutions and persons, and rapidly expanding based on knowledge of client needs and services.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </article>

                                <!-- seventh Services Block -->
                                <article class="col-md-6 col-sm-6 equi-height-each">
                                    <div class="services-each-block section-bgimg">
                                        <div class="services-each-container">
                                            <div class="icon-section">
                                                <i class="ion-paintbrush lighter-black"></i>
                                            </div>
                                            <div class="heading-section">
                                                <h4 class="lighter-black font1 uppercase bold-font">Logo Designing</h4>
                                            </div>
                                            <div class="content-section">
                                                <p class="content-type-one grey font2 normal-font">
                                                    Logo designing is a comprehensive process, beginning with business briefing, and moving on to the creative skills of knowledgeable and skilled logo designers, who best understand both the art and craft of the logo. 
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </article>

                                <!-- seventh Services Block -->
                                <article class="col-md-6 col-sm-6 equi-height-each">
                                    <div class="services-each-block section-bgimg">
                                        <div class="services-each-container">
                                            <div class="icon-section">
                                                <i class="ion-ios-videocam-outline lighter-black"></i>
                                            </div>
                                            <div class="heading-section">
                                                <h4 class="lighter-black font1 uppercase bold-font">Aerial Drone Photo/Videography</h4>
                                            </div>
                                            <div class="content-section">
                                                <p class="content-type-one grey font2 normal-font">
                                                    Suspendisse congue bibendum auctor. Etiam sapien risus, fringilla vehicula viverra eget, eleifend quis elit vtiam accumsan dui efficitur.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </article>

                            </div>
                        </div>
                    </div> 
                </div> 
            </div>

        </section>
        <!-- Services Section Ends -->

        <!-- Intermediate Features Section Starts -->
        <section class="page-section intermediate-features-section section-bgimg background-fixed pad-top pad-bottom" data-sectionBgImg="<?php echo base_url(); ?>assets/images/img/home.jpg">

            <div class="container">
                <div class="row">
                    <article class="col-md-12">
                        <div class="heading-type-one text-center pad-bottom-half">
                            <h2 class="white font2 uppercase normal-font">Nothing EXPECT <span class="main bold-font">Than Success</span> </h2>
                        </div> 
                    </article>
                </div>

                <div class="row">
                    <!-- First Feature Block -->
                    <article class="col-md-6 col-sm-6">
                        <div class="features-each-block black-rgba-high-bg">
                            <div class="heading-section">
                                <h4 class="white font2 normal-font uppercase"><span class="icon-section ease ion-chatbox white"></span>WFX <span class="main bold-font uppercase"> VISION</span></h4>
                            </div>
                            <div class="content-section">
                                <p class="content-type-one light-white font1 normal-font">
                                    to give superior and latest web related services for achiving highest level in web designing feild by working this competitive enviroment

                                </p>
                            </div>
                        </div>
                    </article>

                    <!-- Second Feature Block -->
                    <article class="col-md-6 col-sm-6">
                        <div class="features-each-block black-rgba-high-bg">
                            <div class="heading-section">
                                <h4 class="white font2 normal-font uppercase"><span class="icon-section ease ion-ios-clock white"></span>WFX <span class="main bold-font uppercase">MISION</span></h4>
                            </div>
                            <div class="content-section">
                                <p class="content-type-one light-white font1 normal-font">
                                    To provide Huge support and  Web related facilities for our clients to achive there organizational goals and objectives successfully. 
                                </p>
                            </div>
                        </div>
                    </article>

                </div>

            </div>

        </section>
        <!-- Intermediate Features Section Ends -->

        <!-- Portfolio Section Starts -->
        <section id="portfolio" class="page-section section-fixed-header-wrap top-separator bottom-separator portfolio-section white-bg">

            <!-- Fixed Header Section Starts -->
            <div class="fixed-header fullheight">

                <!-- SVG Section Starts -->
                <svg class="parent" xmlns="https://www.w3.org/2000/svg" version="1.1"  xmlns:xlink="https://www.w3.org/1999/xlink"> 
                <!-- add title for accessibility -->
                <title>Mach SVG</title> 
                <!-- Definition Section Of the SVG -->
                <defs>  
                <clippath id="my-path03">
                    <text class="letter-text font1" y="57%">P</text>
                    <text class="letter-text-ie font1 hidden" x="50%" y="57%">P</text>
                    <!-- <text class="header-text font1" x="50%" y="65%">About Us</text> -->
                </clippath>
                </defs>
                <!-- The background Image for the svg font -->
                <image xlink:href="<?php echo base_url(); ?>assets/images/img/service.jpg" clip-path="url(#my-path03)" width="100%" height="100%" preserveAspectRatio="xMidYMid slice" />
                </svg>
                <!-- SVG Section Ends -->

                <p class="header-text light-more-black font2">Portfolio</p>

            </div>
            <!-- Fixed Header Section Ends -->

            <div class="fixed-aside-section white-bg">
                <div class="vertical-align main-aligner">
                    <div class="content-section pad-top-half pad-bottom-half">
                        <div class="container">
                            <div class="row">
                                <article class="col-md-12">

                                    <!-- Portfolio Content Starts -->

                                    <div class="row" >
                                        <div class="col-md-4 col-xs-6 col-sm-4 portfolio-item hover-block-wrap">
                                            <a href="#" class="img-section hover-block-container feather-single">
                                                <img class="img-responsive" src="<?php echo base_url(); ?>assets/images/portfolio/helahanda.jpg" alt="">
                                                <div class="hover-visible black-rgba-high-bg">
                                                    <h3 class="light-white font2 uppercase bold-font">
                                                        Hela Handa FM
                                                    </h3>
                                                    <h4 class="main font2 uppercase bold-font">helahandafm.lk</h4>
                                                </div>
                                            </a>

                                        </div>
                                        <div class="col-md-4 col-xs-6 col-sm-4 portfolio-item hover-block-wrap">
                                            <a href="#" class="img-section hover-block-container feather-single">
                                                <img class="img-responsive" src="<?php echo base_url(); ?>assets/images/portfolio/firstteam.jpg" alt="">
                                                <div class="hover-visible black-rgba-high-bg">
                                                    <h3 class="light-white font2 uppercase bold-font">
                                                        First Team
                                                    </h3>
                                                    <h4 class="main font2 uppercase bold-font">firstteam.lk</h4>
                                                </div>
                                            </a>

                                        </div>
                                        <div class="col-md-4 col-xs-6 col-sm-4 portfolio-item hover-block-wrap">
                                            <a href="#" class="img-section hover-block-container feather-single">
                                                <img class="img-responsive" src="<?php echo base_url(); ?>assets/images/portfolio/r-entertainment.jpg" alt="">
                                                <div class="hover-visible black-rgba-high-bg">
                                                    <h3 class="light-white font2 uppercase bold-font">
                                                        R-Entertainment
                                                    </h3>
                                                    <h4 class="main font2 uppercase bold-font">rentertainment.lk</h4>
                                                </div>
                                            </a>

                                        </div>
                                        <div class="col-md-4 col-xs-6 col-sm-4 portfolio-item hover-block-wrap">
                                            <a href="#" class="img-section hover-block-container feather-single">
                                                <img class="img-responsive" src="<?php echo base_url(); ?>assets/images/portfolio/ocean-beach.jpg" alt="">
                                                <div class="hover-visible black-rgba-high-bg">
                                                    <h3 class="light-white font2 uppercase bold-font">
                                                        Ocean View Beach Resort
                                                    </h3>
                                                    <h4 class="main font2 uppercase bold-font">oceanviewresort.lk</h4>
                                                </div>
                                            </a>

                                        </div>
                                        <div class="col-md-4 col-xs-6 col-sm-4 portfolio-item hover-block-wrap">
                                            <a href="#" class="img-section hover-block-container feather-single">
                                                <img class="img-responsive" src="<?php echo base_url(); ?>assets/images/portfolio/ceylon-living.jpg" alt="">
                                                <div class="hover-visible black-rgba-high-bg">
                                                    <h3 class="light-white font2 uppercase bold-font">
                                                        ceylon living herityage
                                                    </h3>
                                                    <h4 class="main font2 uppercase bold-font">ceylonlivingherityage.com</h4>
                                                </div>
                                            </a>

                                        </div>
                                        <div class="col-md-4 col-xs-6 col-sm-4 portfolio-item hover-block-wrap">
                                            <a href="#" class="img-section hover-block-container feather-single">
                                                <img class="img-responsive" src="<?php echo base_url(); ?>assets/images/portfolio/letmeapply.jpg" alt="">
                                                <div class="hover-visible black-rgba-high-bg">
                                                    <h3 class="light-white font2 uppercase bold-font">
                                                        LetMeApply
                                                    </h3>
                                                    <h4 class="main font2 uppercase bold-font">letmeapply.lk</h4>
                                                </div>
                                            </a>

                                        </div>


                                    </div>
                                    <!-- Portfolio Content Ends -->

                                </article>
                            </div>
                        </div>
                    </div>  
                </div>  
            </div>

        </section>
        <!-- Portfolio Section Ends -->
        <!-- Intermediate Pricing Section Starts -->

        <section class="page-section white-bg">
            <div class="row">
                <div class="col-md-12 map-div" id="map">

                </div>
            </div>

        </section>
        <!-- Intermediate Pricing Section Ends -->

        <!-- Contact Section Starts -->
        <section id="contact" class="page-section section-fixed-header-wrap contact-section white-bg">

            <!-- Fixed Header Section Starts -->
            <div class="fixed-header fullheight">

                <!-- SVG Section Starts -->
                <svg class="parent" xmlns="https://www.w3.org/2000/svg" version="1.1"  xmlns:xlink="https://www.w3.org/1999/xlink"> 
                <!-- add title for accessibility -->
                <title>Mach SVG</title> 
                <!-- Definition Section Of the SVG -->
                <defs>  
                <clippath id="my-path06">
                    <text class="letter-text font1" y="57%">R</text>
                    <text class="letter-text-ie font1 hidden" x="50%" y="57%">R</text>
                    <!-- <text class="header-text font1" x="50%" y="65%">About Us</text> -->
                </clippath>
                </defs>
                <!-- The background Image for the svg font -->
                <image xlink:href="<?php echo base_url(); ?>assets/images/img/contact.jpg" clip-path="url(#my-path06)" width="100%" height="100%" preserveAspectRatio="xMidYMid slice" />
                </svg>
                <!-- SVG Section Ends -->

                <p class="header-text light-more-black font2">Reach Us</p>

            </div>
            <!-- Fixed Header Section Ends -->

            <div class="fixed-aside-section white-bg">
                <div class="vertical-align main-aligner">
                    <div class="content-section team-content pad-top-half">
                        <!-- Contact Form Section Starts -->

                        <div class="contact-form-section padding-bottom-75 section-bgimg background-fixed">

                            <div class="container">

                                <div class="row">
                                    <article class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">

                                        <!-- Contact Form : starts -->
                                        <div class="">
                                            <!-- contact-form -->

                                            <form id="contactform" name="myform" action="#" enctype="multipart/form-data" method="post">
                                                <input class="black font2 form-control input-txt" type="text" id="name" placeholder="Name:" name="name" data-placeholder="Name">
                                                <input class="black font2 form-control input-txt" type="email" id="email" placeholder="Email:" name="email" data-placeholder="Email">
                                                <input class="black font2 form-control input-txt" type="email" id="phone" placeholder="Phone" name="phone" data-placeholder="Phone">
                                                <textarea class="black font2 form-control input-txt" rows="4" id="message" name="message" placeholder="Message:" data-placeholder="Message"></textarea>
                                                <div class="mach-button-section button-wrap">
                                                    <button class="btn btn-primary submit-btn" type="submit" name="submit" id="submit">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- Contact Form : ends -->

                                    </article>
                                </div>
                            </div>

                        </div>
                        <!-- Contact Form Section Ends -->

                        <!--added new salinda-->

                        <div class="contact-form-section padding-bottom-75 section-bgimg background-fixed">

                            <div class="container">

                                <div class="row">
                                    <article class="col-md-6 col-sm-6">
                                        <div class="features-each-block black-rgba-high-bg no-padding-all">
                                            <div class="heading-section padding-20">
                                                <h4 class="white font2 normal-font uppercase"><span class="icon-section ease ion-ios-home-outline white"></span>WFX <span class="main bold-font uppercase"> Address</span></h4>
                                            </div>
                                            <div class="content-section">
                                                <div class="col-md-6 no-padding background-light-gray">
                                                    <p class="ion-ios-home-outline center-block icon-section ease white text-center font-size-35"></p>
                                                    <p class="content-type-one light-white font1 normal-font text-center">
                                                        No 02, Uyanwaththa Road<br>
                                                        Polgolla, Kandy<br>
                                                        Sri Lanka
                                                    </p>
                                                </div>
                                                <div class="col-md-6 no-padding background-gray div-height">
                                                    <p class="ion-ios-email-outline center-block icon-section ease white text-center font-size-35"></p>
                                                    <p class="content-type-one light-white font1 normal-font text-center">
                                                        suport@wfxlive.com
                                                    </p>
                                                </div>
                                                <div class="clearfix"></div>


                                            </div>
                                        </div>
                                    </article>
                                    <article class="col-md-6 col-sm-6">
                                        <div class="features-each-block black-rgba-high-bg no-padding-all">
                                            <div class="heading-section padding-20">
                                                <h4 class="white font2 normal-font uppercase">
                                                    <span class="icon-section ease ion-ios-telephone-outline white"></span>
                                                    WFX 
                                                    <span class="main bold-font uppercase"> Contacts</span></h4>
                                            </div>
                                            <div class="content-section">
                                                <div class="col-md-4 no-padding background-light-gray padding-top-bottom">
                                                    <p class="ion-person center-block icon-section ease white text-center font-size-35"></p>
                                                    <p class="content-type-one light-white font1 normal-font text-center">
                                                        +94 77 786 6034 <br> Thilina
                                                    </p>
                                                </div>
                                                <div class="col-md-4 no-padding background-gray padding-top-bottom">
                                                    <p class="ion-person center-block icon-section ease white text-center font-size-35"></p>
                                                    <p class="content-type-one light-white font1 normal-font text-center">
                                                        +94 71 745 0109 <br> Madawa
                                                    </p>
                                                </div>
                                                <div class="col-md-4 no-padding background-light-gray padding-top-bottom">
                                                    <p class="ion-person center-block icon-section ease white text-center font-size-35"></p>
                                                    <p class="content-type-one light-white font1 normal-font text-center">
                                                        +94 71 388 2815 <br> Dhanushka
                                                    </p>
                                                </div>

                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                        <!--end added new salinda-->

                    </div>  
                </div>  
            </div>

        </section>
        <!-- Contact Section Ends -->

