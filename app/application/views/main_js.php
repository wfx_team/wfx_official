<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Custom-JavaScript-File-Links -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ="crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/moment-timezone-with-data.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/timer.js"></script>
<!-- //Custom-JavaScript-File-Links -->